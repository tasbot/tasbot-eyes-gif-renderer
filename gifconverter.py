from PIL import Image, ImageSequence
import time
import board
import neopixel
import signal
import sys
import argparse
import os.path
import numpy as np

pixels = None
original_sigint = None
positionArray = None
im = None

def loop_gif(x_offset, y_offset):
    #  Offset visualized:
    #   ____________________________________________
    #  |              ^                             |
    #  |              |                             |
    #  |              | y-offset                    |
    #  |              |                             |
    #  |   x-offset   v_________________            |
    #  |<----------->|        eye       |           |
    #  |             |   display_area   |           |
    #  |             |_________________ |           |
    #  |                                            |
    #  |                                            |
    #  |                                            |
    #  |                                            |
    #  |____________________________________________|
    


    x_offset = x_offset
    y_offset = y_offset
    while True:
        for frame in ImageSequence.Iterator(im):
            # print (frame.info)
            duration = frame.info['duration']
            time_end  = time.time() + (duration/1000)
            while time.time() < time_end:
                cur_frame = frame.convert('RGBA')

                for x in range(x_offset, x_offset + 28):
                    
                    for y in range(y_offset, y_offset + 8):
                        
                        r, g, b, a = cur_frame.getpixel((x,y))

                        if positionArray[y-y_offset][x-x_offset] != 'x':
                            if a > 0:
                                pixels[positionArray[y-y_offset][x-x_offset]] = (r,g,b)
                            else:
                                pixels[positionArray[y-y_offset][x-x_offset]] = (0,0,0)
                
                pixels.show()

def show_image(input_file, x_offset, y_offset, show_time):
    while True:
        
        for x in range(0, im.width):
            
            for y in range(0, im.height):
                
                r, g, b, a = im.getpixel((x,y))
                x_pos = x + x_offset
                y_pos = y + y_offset
                # print (y_pos)
                try:
                    if positionArray[y+y_offset][x+x_offset] != 'x':
                        if a > 0:
                            pixels[positionArray[y][x+x_offset]] = (r,g,b)
                        else:
                            pixels[positionArray[y][x+x_offset]] = (0,0,0)
                except:
                    pass
                
            
        pixels.show()
        if show_time == False:
            break


def loop_image(loop):
    x_pos = 2 - im.width
    y_pos = 0 
    
    while True:
        # print(y_pos)
        if y_pos > im.height:
            print(y_pos)
            x_pos = 0 - im.width
        else:
            show_image(im, x_pos, y_pos, False)
            time.sleep(2)
            x_pos += 1
            clear_leds()


def clear_leds():
    pixels.fill((0,0,0))
    pixels.show()

def setup_leds(brightness, order):
    # This is physical pin 12 and GPIO18
    pixel_pin = board.D18

    # The number of leds in the eyes
    num_pixels = 154

    # The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
    # For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
    if order == "grb":
        ORDER = neopixel.GRB
    elif order == "rgb":
        ORDER = neopixel.RGB
    elif order == "grbw":
        ORDER = neopixel.GRBW
    elif order == "rgbw":
        ORDER = neopixel.RGBW
    else:
        print(f"{order} not recognized. Falling back to GRB.")
        ORDER = neopixel.GRB

    global pixels

    pixels = neopixel.NeoPixel(
        pixel_pin, num_pixels, brightness=brightness, auto_write=False, pixel_order=ORDER
    )

    global positionArray
    positionArray = [
        ['x','x',0,1,2,3,'x','x','x','x',101,100,99,98,97,96,95,94,'x','x','x','x',105,104,103,102,'x','x','x','x'],
        ['x',4,5,6,7,8,9,'x','x',84,85,86,87,88,89,90,91,92,93,'x','x',111,110,109,108,107,106,'x','x','x'],
        [10,11,12,13,14,15,16,17,'x','x','x','x','x','x','x','x','x','x','x','x',119,118,117,116,115,114,113,112,'x','x'],
        [18,19,20,21,22,23,24,25,'x','x','x',83,82,81,80,79,78,'x','x','x',127,126,125,124,123,122,121,120,'x','x'],
        [26,27,28,29,30,31,32,33,'x','x',70,71,72,73,74,75,76,77,'x','x',135,134,133,132,131,130,129,128,'x','x'],
        [34,35,36,37,38,39,40,41,'x','x','x','x','x','x','x','x','x','x','x','x',143,142,141,140,139,138,137,136,'x','x'],
        ['x',42,43,44,45,46,47,'x','x','x',68,67,66,65,64,63,62,61,'x','x','x',149,148,147,146,145,144,'x','x','x'],
        ['x','x',48,49,50,51,'x','x','x',69,52,53,54,55,56,57,58,59,60,'x','x','x',153,152,151,150,'x','x','x','x'],
    
    ]

def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)
    pixels.deinit()
    sys.exit(1)

    # restore the exit gracefully handler here    
    signal.signal(signal.SIGINT, exit_gracefully)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Control TASBot\'s eyes by supplying an image!')
    parser.add_argument("brightness",  type = float)
    parser.add_argument("x_offset", type = int)
    parser.add_argument("y_offset", type = int)
    parser.add_argument("input_file", type = str)
    parser.add_argument("-l", "--loop", type = str)
    parser.add_argument("-o", "--order", type = str, default = "grb")

    args = parser.parse_args()

    if not os.path.isfile(args.input_file):
        print(f'ERROR: No file "{args.input_file}" found.')
        sys.exit(0)
    if not args.input_file.endswith((".gif", ".png")):
        print(f"ERROR: {args.input_file} is not a valid image")
        sys.exit(0)

    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)


    setup_leds(args.brightness, args.order)
    
    im = Image.open(args.input_file) 

    if ".gif" in args.input_file:
        loop_gif(args.x_offset, args.y_offset)
    elif args.loop:
        loop_image(args.loop)
    else:
        show_image(im, args.x_offset, args.y_offset, True)
    




    
